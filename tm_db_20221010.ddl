BEGIN;

CREATE DATABASE tm
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE SCHEMA tm
    AUTHORIZATION postgres;

CREATE TABLE IF NOT EXISTS tm.tm_project
(
    id character varying(100) NOT NULL,
    created timestamp(0) with time zone,
	name character varying(150),
    description character varying(250),
	status character varying(20),
    user_id character varying(100),      
    PRIMARY KEY (id)
);

COMMENT ON TABLE tm.tm_project
    IS 'Project list';

CREATE TABLE IF NOT EXISTS tm.tm_task
(
    id character varying(100) NOT NULL,
    created timestamp(0) with time zone,
	name character varying(150),
    description character varying(250),
	status character varying(20),
    user_id character varying(100),   
    project_id character varying(100),
    PRIMARY KEY (id)
);

COMMENT ON TABLE tm.tm_task
    IS 'Task list';

CREATE TABLE IF NOT EXISTS tm.tm_session
(
    id character varying(100) NOT NULL,
    date timestamp with time zone,
    user_id character varying(100),
    role character varying(15),
    PRIMARY KEY (id)
);

COMMENT ON TABLE tm.tm_session
    IS 'Session list';

CREATE TABLE IF NOT EXISTS tm.tm_user
(
    id character varying(100) NOT NULL,
	created timestamp(0) with time zone,
	first_name character varying(100),
    last_name character varying(100),
    middle_name character varying(100),
    login character varying(50) NOT NULL,
    password character varying(500) NOT NULL,
    email character varying(100),
    locked boolean,    
	role character varying(15),
    PRIMARY KEY (id)
);

COMMENT ON TABLE tm.tm_user
    IS 'User list';
END;