package ru.t1.nikitushkina.tm.dto.response;

import org.jetbrains.annotations.NotNull;

public final class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse() {
        setSuccess(false);
    }

    public ApplicationErrorResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
