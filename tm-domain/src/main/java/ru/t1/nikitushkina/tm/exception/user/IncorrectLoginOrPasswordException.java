package ru.t1.nikitushkina.tm.exception.user;

public final class IncorrectLoginOrPasswordException extends AbstractUserException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Incorrect Login or password.");
    }

}
