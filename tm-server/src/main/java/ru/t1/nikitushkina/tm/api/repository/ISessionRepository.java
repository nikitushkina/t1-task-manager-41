package ru.t1.nikitushkina.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm.tm_session (id, date, user_id, role)" +
            " VALUES (#{id}, #{date}, #{userId}, #{role})")
    void add(@NotNull SessionDTO session);

    @Insert("INSERT INTO tm.tm_session (id, date, user_id, role)" +
            " VALUES (#{id}, #{date}, #{userId}, #{role})")
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull SessionDTO session);

    @Delete("DELETE FROM tm.tm_session WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm.tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<SessionDTO> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm.tm_session " +
            " WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable SessionDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm.tm_session WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable SessionDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(1) FROM tm.tm_session WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm.tm_session WHERE user_id = #{userId} AND id = #{id}")
    void remove(@NotNull SessionDTO session);

    @Update("UPDATE tm.tm_session " +
            " SET name = #{name}, date = #{date}, role = #{role} " +
            " WHERE id = #{id}")
    void update(@NotNull SessionDTO session);

}
